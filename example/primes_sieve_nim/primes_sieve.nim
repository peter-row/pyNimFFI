import PyNimFFI
# http://stackoverflow.com/questions/3939660/sieve-of-eratosthenes-finding-primes-python

proc primes_sieve_nim(limit:int):seq[int] =
  var a:seq[bool] = @[]
  a.setLen(limit) # Initialize the primality list
  for i in 0..limit-1:
    a[i]=true
  a[0] = false
  a[1] = false
  for i in 0..limit-1:
    if a[i]:
      var n = i*i
      while n < limit:
        a[n] = false
        n = n + i
  result = @[]
  for i in 0..limit-1:
    if a[i]:
      result.add(i)

proc primes_sieve(limit:int):seq[int] {.pyFuncSafe.} =
  let res = primes_sieve_nim(limit)
  res

proc largest_prime(limit:int):int {.pyFuncSafe.} =
  let primes = primes_sieve_nim(limit)
  result = primes[primes.high]

bootstrap()