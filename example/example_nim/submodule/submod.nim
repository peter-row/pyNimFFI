import PyNimFFI
import strutils

type
  myObj = tuple[s:string]



pyClass myObj:
  proc repr(self: ref myObj): cstring =
    result = self.s
  
  proc repr2(self: ref myObj): seq[cstring] =
    result = @[]
    result.add(self.s)
    result.add("world")

proc hello():cstring {.pyFuncRaw.} =
  "Helo world"

proc hello2(self: ref myObj):cstring {.pyFuncSafe.} =
  result = self.s

proc create(s:cstring):ref myObj {.pyFuncRaw.}=
  result = new(myObj)
  result.s = $s
  
proc makeBigString(i:int): ref myObj{.pyFuncRaw.}=
  result = new(myObj)
  result.s = $('&'.repeat(i))
  

proc makeBigStrings(i:int,j:int): seq[ref myObj]{.pyFuncSafe.}=
  result = @[]
  for jj in 0..j-1:
    let res = new(myObj)
    res.s = $('&'.repeat(i))
    result.add(res)

proc echoRaw(s:seq[cstring]):seq[cstring] {.pyFuncRaw.}=
  result = @[]
  for i in s:
    result.add(i)

proc echoSafe(s:seq[cstring]):seq[cstring] {.pyFuncSafe.}=
  result = @[]
  for i in s:
    result.add(i)
    
