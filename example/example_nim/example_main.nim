import PyNimFFI
import submodule.submod

proc innerFunc(i:int):float =
  10.0

proc doit(i:int):float {.pyFuncRaw.} =
  innerFunc(i)

proc hello():cstring {.pyFuncRaw.} =
  "Helo world"

proc hello2():seq[cstring] {.pyFuncRaw.} =
  result = @[]
  result.add("Hello")
  result.add("world")

proc hello3():cstring {.pyFuncSafe.} =
  "Helo world"

proc hello4():seq[cstring] {.pyFuncSafe.} =
  result = @[]
  result.add("Hello")
  result.add("world")

let x:float = doit(3)

proc doit2(i:int,j:float):float {.pyFuncRaw.} =
  var res: Safe[float]
  try:
    res.result = 20.0
    res.has_err = false
    res.msg = ""
  except:
    res.has_err=true
    res.msg = getCurrentExceptionMsg()
  result = res.result
  
proc doit3(i:int):float {.pyFuncSafe.} =
  assert i>0
  return 30.0

type
  myObj = distinct int

pyClass myObj:
  discard

var arg = new(myObj)
arg[] = 5.myObj

proc doit6(i: ref myObj): int {.pyFuncSafe.} = 
  i[].int

proc doit7(i: int): ref myObj {.pyFuncRaw.} = 
  result = new(myObj)
  result[] = i.myObj

let v= doit7(4)

echo v.result[].int

proc doit9(i: int): seq[int]{.pyFuncSafe.} = 
  var res:seq[int] = @[]
  for j in 0..i-1:
    res.add(j)
  result = res

let w = doit9(10)
#  i.len

proc doit10(i: seq[int]):int {.pyFuncRaw.} = 
  result = 0
  for j in i:
    result+=j

var test_seq : RawArray[int] = new(RawArray[int])
test_seq.len=3
test_seq.data[0..3]=[0,4,6,8888]

proc doit10b(i: seq[int]):int {.pyFuncSafe.} = 
  result = 0
  for j in i:
    result+=j

proc doit12(i: int): seq[ref myObj] {.pyFuncSafe.} = 
  var res:seq[ref myObj] = @[]
  res.setLen(i)
  for j in 0..i-1:
    let res2 = new(myObj)
    res2[] = j.myObj
    res[j] = (res2)
  result = res

proc doit13(i: seq[ref myObj]): int {.pyFuncSafe.} = 
  result = 0
  for j in i:
    result+=j[].int

bootstrap()
