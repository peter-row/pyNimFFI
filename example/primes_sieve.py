import sys
sys.path.append('..')

try: xrange
except: xrange = range

# http://stackoverflow.com/questions/3939660/sieve-of-eratosthenes-finding-primes-python
def primes_sieve(limit):
    a = [True] * limit                          # Initialize the primality list
    a[0] = a[1] = False

    for (i, isprime) in enumerate(a):
        if isprime:
            yield i
            for n in xrange(i*i, limit, i):     # Mark factors non-prime
                a[n] = False

def largest_prime(limit):
    for prime in primes_sieve(limit):
        result = prime
    return result

import sys
from PyNimFFI import nim_cdef, nim_wrap
from cffi import FFI
output_f = 'primes_sieve_nim/seqdefs.json'
ffi = FFI()
nim_cdef(ffi,output_f)
C = ffi.dlopen('primes_sieve_nim/libprimes_sieve.dylib')
nim_wrap(ffi, C, output_f)
C.NimMain()

def primes_sieve_nim(limit):
    import _primes_sieve
    return _primes_sieve.primes_sieve(limit)

def largest_prime_nim(limit):
    import _primes_sieve
    return _primes_sieve.largest_prime(limit)

if __name__ == '__main__':
    import timeit
    print("Python prime sieve")
    print(list(primes_sieve(10)))
    print(timeit.timeit("from __main__ import primes_sieve;list(primes_sieve(10**3))",number=3))
    print(timeit.timeit("from __main__ import primes_sieve;list(primes_sieve(10**6))",number=3))
    print("Nim prime sieve")
    print(primes_sieve_nim(10))
    print(timeit.timeit("from __main__ import primes_sieve_nim;list(primes_sieve_nim(10**3))",number=3))
    print(timeit.timeit("from __main__ import primes_sieve_nim;list(primes_sieve_nim(10**6))",number=3))
    print("Python largest prime")
    print(largest_prime(10**6))
    print(timeit.timeit("from __main__ import largest_prime;(largest_prime(10**6))",number=3))
    print(timeit.timeit("from __main__ import largest_prime;(largest_prime(10**7))",number=3))
    print("Nim largest prime")
    print(largest_prime_nim(10**6))
    print(timeit.timeit("from __main__ import largest_prime_nim;(largest_prime_nim(10**6))",number=3))
    print(timeit.timeit("from __main__ import largest_prime_nim;(largest_prime_nim(10**7))",number=3))

