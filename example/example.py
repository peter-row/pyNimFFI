import sys
from PyNimFFI import nim_cdef, nim_wrap
from cffi import FFI
output_f = 'example_nim/seqdefs.json'
ffi = FFI()
nim_cdef(ffi,output_f)
C = ffi.dlopen('example_nim/libexample_main.dylib')
nim_wrap(ffi, C, output_f)

C.NimMain()

import _example_main as example_main
print
print 'testing!!!'
print example_main.hello()
print example_main.hello2()
print example_main.hello3()
print example_main.hello4()

print example_main.doit(1)
print example_main.doit2(1,2)
print example_main.doit3(1)
try:
    print example_main.doit3(-2)
    raise NotImplementedError("no error propagation")
except Exception,e:
    print e
    print 'good'
print example_main.doit7(10)
v = example_main.doit7(9001)
print example_main.doit6(v)
print example_main.doit9(20)
print example_main.doit10([1,2,993])
print example_main.doit10([1,2,3,7,8,33])
print example_main.doit12(6)
l = example_main.doit12(67)
print example_main.doit13(l[23:99])
import sys
import _submodule#.submod as submod

import _submodule.submod
c = _submodule.submod.create('string!')
print 'Here',c
print _submodule.submod.hello()
print _submodule.submod.hello2(c)

print c.repr()
print c.repr2()

print _submodule.submod.echoRaw(['1','2','3','4'])
print _submodule.submod.echoSafe(['1','2','3','4'])

# check memory use
#for i in range (1000):
#    l = _submodule.submod.makeBigString(50*1000*1000)
#    print l
for i in range (1000):
    l = _submodule.submod.makeBigStrings(30*1000*1000,5)
    print len(l),[len(s.repr()) for s in l]
