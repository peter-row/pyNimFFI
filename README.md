in nim nim_src/module.nim :

    import PyNimFFI
    import strutils

    type
      myObj = tuple[s:string
      
    # create a class
    pyClass myObj:
      proc repr(self: ref myObj): cstring =
        result = self.s
    
    proc create(s: cstring): ref myObj {.pyFuncRaw.} =
        result = new(myObj)
        result[].s = s
    
    # create a function
    proc hello():cstring {.pyFuncRaw.} =
      "Helo world"
    
    # errors can be automatically propagated
    proc hello(i:int):cstring {.pyFuncSafe.} =
      assert i>0
      "Hello World"
    
    # macro to write the definitions out, so Python can read them.
    bootstrap() 

in Python:

    from PyNimFFI import nim_cdef, nim_wrap
    from cffi import FFI
    # where the fi
    output_f = 'nim_src/seqdefs.json'
    ffi = FFI()
    nim_cdef(ffi,output_f)
    C = ffi.dlopen('nim_src/libexample_main.dylib')
    nim_wrap(ffi, C, output_f)
    
    # do this if you don't want a segfault
    C.NimMain()
    
    # fake modules are created
    import _submod
    obj = _submod.create("hello world")
    print obj.repr()