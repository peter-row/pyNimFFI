[Package]
name          = "PyNimFFI"
version       = "0.1.0"
author        = "Peter Row"
description   = "A wrapper for Python calling Nim"
license       = "MIT"
srcDir = "PyNimFFI"
bin = "PyNimFFIstaticWrite"

[Deps]
Requires: "nim >= 0.11.0"
