#The MIT License (MIT)

#Copyright (c) 2015 Peter Row

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.

##Copyright (c) 2015 Peter Row
## MIT Licensed

## To skip the boring stuff, go straight to the pyClass macro section.
##
## This project helps generate a simple cffi-based wrapper for nim code, to be 
## used in Python.
##
## Here's a rough outline of what happens when you add a `pyFuncSafe` or 
## `pyFuncRaw` macro to your proc.
## * The macro will capture the current file location
## * Export pragmas are added, to tell the compile to export the proc.
## * The arguments of the proc are re-written, and converters created. This is to \
## standardise the function's signature, so that it's easier to import / process.
## * If you use `pyFuncSafe`, it will wrap the function in a try / except \
## statement, and return a structure that allows errors to be propagated into \
## Python exceptions.
## * If you use `pyFuncRaw`, it will ensure no exceptions can be raised, and \
## the compiler will fail if you allow exceptions. 
## * Note to Pythoneers, SEFAULTS will not be propagated, \
## and are possible in nim (usually when you try to add an item to an uninitialised \
## sequence or table, which are NULL pointers).

import macros,tables,strutils,PyNimUtils
type
  bstring* = tuple[len:int,buffer:cstring]
  Raw*[T: int|cstring|bstring|float] = T ##\
  ## Raw and RawRef are passed from Python to nim, and from nim to Python.\
  ## basic arguments / results are not converted 
  
  RawRef*[T] = tuple [nim_class: int, result:ref T] ## \
  ## All classes originate in nim, but you can pass them from nim, to\
  ## Python, then back to nim. This is obviously useful for methods. 
  ## Classes / tuples must be passed in and out by opaque reference. The `nim_class`\
  ## ensures if you pass a reference  of the wrong type from Python to nim, \
  ## it can bail out gracefully. Were not for this safeguard, you 
  ## might be able to convince nim that a memory location contains a completely \
  ## different object, leading to probable segfaults (if you're lucky).
  
  Safe*[T: int|cstring|bstring|float] = tuple [has_err:bool, msg:c_string, result:T] ##\
  ## Safe results are only passed from nim to Python. They allow exceptions to be caught\
  ## and propagated.
  SafeRef*[T] = tuple [has_err:bool, msg:c_string, result:RawRef[T]]
  
  SafeSeq*[T: int|cstring|bstring|float] = tuple [has_err:bool, msg:c_string, result:seq[T]]
  SafeRefSeq*[T] = tuple [has_err:bool, msg:c_string, result:seq[RawRef[T]]] ##\
  ## There is no raw sequence, only safe sequnces. The overhead
  ## of sequences is pretty high (high enough that a couple of try / excepts
  ## is marginal, compared to python making multiple function calls, etc)
  ## and there were memory leaks and missing cstring bugs.
    
  RawArray*[T: int|cstring|bstring|float] = ref tuple[len:int,data:array[0..MAX_ARRAY-1, T]]
  RawRefArray*[T] = ref tuple[len:int,data:array[0..MAX_ARRAY-1, RawRef[T]]] ##\
  ## Python -> nim sequences. Note, there is a limit to how many items you can pass.

template make_unwrap(t:typedesc) :stmt {.immediate.} = 
  converter unwrap*(x:RawArray[t]): seq[t] =
    var res = newSeq[t](x.len)
    for ii in 0..x.len-1:
      res[ii]=x.data[ii]
    res

make_unwrap(int)
make_unwrap(cstring)
make_unwrap(bstring)
make_unwrap(float)

var classnameFromInt {.compiletime.}: seq[string] = @[]
var funcTable {.compiletime.} = initTable[string,funcSpec]()
var methTable {.compiletime.} = initTable[string,string]()

template doInnerRaw[T](result: Raw[T]|RawRef[T], inner_f: expr)  = 
  result = inner_f

template doInnerSafe[T](result: Safe[T]|SafeRef[T]|SafeRef[T]|SafeRefSeq[T], inner_f: expr)  = 
  try:
    let res = inner_f
    result.result = res
    result.has_err = false
    result.msg = ""
  except:
    result.has_err = true
    let info = instantiationInfo()
    let line = $(info.line)
    let fname = info.filename
    let err_msg = getCurrentExceptionMsg()
    let msg = "("&fname&":"&line&") "&err_msg
    result.msg = msg

proc modpath(s:string,j:string):string {.compiletime.} =
    var modpath = s.split({'.','/'})
    assert modpath[modpath.high].startswith("nim")
    modpath = modpath[0..modpath.high-1]
    result = modpath.join(j)

proc getNameSpace(n:NimNode): string {.compiletime.} =
  let lib_path = lineinfo(n)
  modpath(lib_path,".")

proc pyFunc(p: NimNode,want_raw:bool,class_name = ""):NimNode {.compiletime.} =
  result = newStmtList()
  expectKind(p, nnkProcDef)
  let lib_name = getNameSpace(p)
  let name = lib_name&"."&($name(p))
  var resultType : ArgSpec
  if (params(p)[0].kind!=nnkEmpty):
    resultType = paramsToResSpec(params(p),want_raw)
  let wrapType = resultType.wrapType
  
  let myParamsSpec = paramsToSpecs(params(p))
  let myParams = convertParams(resultType,myParamsSpec,params(p))
  
  # make the original function an inner function.
  let innerName = ident($(p.name)&"___inner")#genSym(nskProc,"inner")
  var innerFunc = newProc()
  innerFunc.name = innerName
  innerFunc.params = p.params
  innerFunc.body = p.body
  innerFunc.pragma = parseExpr("{.gensym, inline.}")
  
  let myArgList = getArgList(myParamsSpec,params(p))
  
  # add the function details to a table, to be handed to Python.
  assert (not funcTable.has_key(name))
  funcTable[name] = (resultType, myParamsSpec)
  if class_name.len>0:
    methTable[name] = class_name
  
  # write the wrapper function, exported to c
  var myFunc = newProc()
  myFunc.name = p.name
  myFunc.params = myParams
  myFunc.body = case wrapType:
  of wRaw,wArray: newStmtList(getAst(doInnerRaw(ident("result"),newCall(innerName,myArgList))))
  of wSafe,wSafeSeq: newStmtList(getAst(doInnerSafe(ident("result"),newCall(innerName,myArgList))))
  let pragma_str = "{.exportc:\"$1\", dynlib, raises:[].}"%name.replace(".","__")
  myFunc.pragma = parseExpr(pragma_str)
  
  # add the inner function and wrapper function to the result.
  result.add(innerFunc)
  result.add(myFunc)

template converters(t:typedesc,i:int,n:string,c_classname_str:string): stmt =
  ## Class converters
  converter unwrap*(x:RawRef[t]): ref t =
    if i != x.nim_class:
      raise newException(ValueError, "Could not convert to $1 got $2 expected $3"%[n,$(x.nim_class),$i])
    x.result
    
  converter wrap*(x:ref t): RawRef[t] =
    var res: RawRef[t]
    GC_ref(x)
    res.result = x
    res.nim_class= i
    res
  
  # if Python deletes the class, decrease its reference count.
  proc unref*(x:RawRef[t]) {.exportc:"del__"&c_classname_str, dynlib, raises:[].} =
    try:
      let y: ref t = x
      GC_unref(y)
    except:
      echo getCurrentExceptionMsg()
      echo "ERROR! - unable to convert during unref, expect a SEGFAULT"
  
  converter unwrap*(x:RawRefArray[t]): seq[ref t] =
    var res = newSeq[ref t](x.len)
    for ii in 0..x.len-1:
      res[ii]=x.data[ii]
    res
    
  converter wrap*(x:seq[ref t]):seq[RawRef[t]] =
    var res: seq[RawRef[t]] = @[]
    res.setLen(x.len)
    for ii in 0..x.len-1:
      res[ii]=x[ii]
    res

macro pyClass*(head: expr, body: stmt): stmt {.immediate.} =
  assert (head.kind == nnkIdent)
  let lib_name = getNameSpace(head)
  let i = len(classnameFromInt)
  let classname_str = lib_name&"."&($head)
  let c_classname_str = classname_str.replace(".","__")
  classnameFromInt.add(classname_str)
  result = getAst(converters(head,i,classname_str,c_classname_str))
  for node in body.children:
    if node.kind == nnkProcDef:
      result.add(pyFunc(node,false,classname_str))
  return result

macro pyFuncRaw*(p: expr):stmt =
  pyFunc(p,true)

macro pyFuncSafe*(p: expr):stmt =
  pyFunc(p,false)

proc toJson(s:seq[string]):string {.compiletime.} =
  result = "["
  var rows: seq[string] = @[]
  for n in s:
    assert validIdentifier(n.replace(".","__"))
    rows.add("\""&n&"\"")
  result.add(rows.join(","))
  result.add("]")
  
proc toJson(s:ArgSpec):string{.compiletime.}=
  result = "[\"$1\",\"$2\"]"%[$(s.wrapType),$(s.argType)]

proc toJson(s:seq[ParamSpec]):string{.compiletime.}=
  result = "["
  var rows: seq[string] = @[]
  for r in s:
    rows.add("[\"$1\",$2]"%[r.name,toJson(r.spec)])
  result.add(rows.join(","))
  result.add("]")

proc toJson(s:funcSpec):string {.compiletime.}=
  result = "["
  result.add(toJson(s.resultSpec)&",")
  result.add(toJson(s.paramSpecs)&"]")

proc toJson(t:Table[string,funcSpec]):string {.compiletime.} =
  result = "{"
  var rows: seq[string] = @[]
  for n in t.keys:
    assert validIdentifier(n.replace(".","__"))
    rows.add("\""&n&"\":"&toJson(t[n]))
  result.add(rows.join(","))
  result.add("}")

proc toJson(t:Table[string,string]):string {.compiletime.} =
  result = "{"
  var rows: seq[string] = @[]
  for n in t.keys:
    assert validIdentifier(n.replace(".","__"))
    let c = t[n]
    assert validIdentifier(c.replace(".","__"))
    rows.add("\""&n&"\":\""&c&"\"")
  result.add(rows.join(","))
  result.add("}")

macro bootstrap*():stmt=
  # build json
  echo "bootstrapping"
  var s = "{\"funcTable\":$1,\"methTable\":$2,\"classnameFromInt\":$3}"
  s = s%[toJson(funcTable),toJson(methTable),toJson(classnameFromInt)]
  echo "writing out "&s
  echo "to file "&"seqdefs.json"
  echo gorge("PyNimFFIstaticWrite", "seqdefs.json\n"&s)
