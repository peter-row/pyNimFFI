#The MIT License (MIT)

#Copyright (c) 2015 Peter Row

#Permission is hereby granted, free of charge, to any person obtaining a copy
#of this software and associated documentation files (the "Software"), to deal
#in the Software without restriction, including without limitation the rights
#to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
#copies of the Software, and to permit persons to whom the Software is
#furnished to do so, subject to the following conditions:

#The above copyright notice and this permission notice shall be included in
#all copies or substantial portions of the Software.

#THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
#IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
#FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
#AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
#LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
#OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
#THE SOFTWARE.


## A whole load of twisted meta-programming.

import macros, tables,strutils
const MAX_ARRAY* = 1000

type
  WrapType* = enum
    wRaw, wSafe, wSafeSeq, wArray
    
  ArgType* = enum
    aInt, aFloat, aStr, aBytes, aRef
  
  ArgSpec* = tuple[wrapType:WrapType, argType:ArgType]
  ParamSpec* = tuple[name:string,spec:ArgSpec]
  funcSpec* = tuple[resultSpec: ArgSpec, paramSpecs:seq[ParamSpec]]

proc toSpec(n: NimNode,raw:bool,isResult:bool): ArgSpec {.compiletime.} =
  if n.kind == nnkBracketExpr:
    assert ($(n[0])=="seq")
    let r_inner = toSpec(n[1],raw,isResult)
    if isResult:
      result.wrapType = wSafeSeq
    else:
      result.wrapType = wArray
    result.argType = r_inner.argType
    return result
  if raw:
    result.wrapType = wRaw
  else:
    result.wrapType = wSafe
  if n.kind == nnkRefTy:
    result.argType = aRef
    return result
  expectKind(n,nnkIdent)
  case $n
  of "int": result.argType = aInt
  of "float": result.argType = aFloat
  of "cstring": result.argType = aStr
  of "bstring": result.argType = aBytes
  else: raise newException(ValueError,"Should be int, float, cstring, bstring or ref. Can't handle "&($n))

proc paramsToSpecs*(n: NimNode):seq[ParamSpec] {.compiletime.} =
    result = @[]
    expectKind(n,nnkFormalParams)
    for i in 1..(n.len-1):
      expectKind(n[i][0],nnkIdent)
      let name = $n[i][0]
      let spec = to_spec(n[i][1],raw=true,isResult=false)
      result.add((name:name,spec:spec))

proc paramsToResSpec*(n:NimNode,raw:bool):ArgSpec {.compiletime.} =
  expectKind(n,nnkFormalParams)
  result = toSpec(n[0],raw,isResult=true)

proc toWrappedNode(a:ArgSpec,n:NimNode):NimNode {.compiletime.} =
  var typeNode, wrapNode: NimNode
  
  if a.argType == aRef:
    wrapNode = case a.wrapType
      of wRaw: ident("RawRef")
      of wSafe: ident("SafeRef")
      of wArray: ident("RawRefArray")
      of wSafeSeq: ident("SafeRefSeq")
    
    if (a.wrapType == wArray) or (a.wrapType == wSafeSeq):
      typeNode = ident($(n[1][0]))
    else:
      typeNode = ident($(n[0]))
  else:
    wrapNode = case a.wrapType
      of wRaw: ident("Raw")
      of wSafe: ident("Safe")
      of wArray: ident("RawArray")
      of wSafeSeq: ident("SafeSeq")

    typeNode = case a.argType:
      of aInt, : ident("int")
      of aFloat: ident("float")
      of aStr: ident("cstring")
      of aBytes: ident("bstring")
      of aRef: ident("Can't get here")#.getType()
  
  result = newNimNode(nnkBracketExpr)
  result.add(wrapNode)
  result.add(typeNode)

proc convertParams*(resultType: ArgSpec, specs:seq[ParamSpec], n: NimNode):NimNode {.compiletime.} =
  result = newNimNode(nnkFormalParams)
  result.add(resultType.toWrappedNode(n[0]))
  var i = 1
  if specs.len>0:
    for s in specs:
      var identNode = newNimNode(nnkIdentDefs)
      identNode.add(n[i][0])
      identNode.add(s.spec.toWrappedNode(n[i][1]))
      identNode.add(n[i][2])
      result.add(identNode)
      inc(i)

proc getArgList*(specs:seq[ParamSpec], n: NimNode):seq[NimNode] {.compiletime.} =
  result = @[]
  var i = 0
  for s in specs:
    result.add(n[i+1][0])
    inc(i)